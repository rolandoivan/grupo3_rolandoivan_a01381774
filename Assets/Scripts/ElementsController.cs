﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElementsController : MonoBehaviour
{
    public float speed = 3.0f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (GameObject.FindGameObjectWithTag("Rock").transform.position.y > -5)
        {
            GameObject.FindGameObjectWithTag("Rock").transform.Translate(new Vector3(0, -1, 0) * speed * Time.deltaTime);
        }
        else
        {
            Destroy(GameObject.FindGameObjectWithTag("Rock"));
        }
    }
}
