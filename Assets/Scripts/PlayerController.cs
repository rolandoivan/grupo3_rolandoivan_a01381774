﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Threading;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    public Text textoContador;
    private int puntuacion = 0;
    public float speed = 3.0f;

    IEnumerator MyCoroutine()
    {
        GameObject.Find("Player").transform.localScale += new Vector3(2F, 0, 0);
        yield return new WaitForSeconds(6);
        GameObject.Find("Player").transform.localScale += new Vector3(-2F, 0, 0);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Rock")
        {
            Destroy(other.gameObject);
            puntuacion = puntuacion + 1;
            textoContador.text = puntuacion.ToString();
        }
        if (other.gameObject.tag == "Power")
        {
            Destroy(other.gameObject);
            StartCoroutine(MyCoroutine());
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (GameObject.Find("Player").transform.position.x < 7.2 && GameObject.Find("Player").transform.position.x > -7.2)
        {
            transform.Translate(new Vector3(Input.GetAxis("Horizontal"), 0, 0) * speed * Time.deltaTime);
        }
        else if(GameObject.Find("Player").transform.position.x >= 7.2)
        {
            if (Input.GetKey(KeyCode.LeftArrow))
            {
                transform.Translate(new Vector3(-1, 0, 0) * speed * Time.deltaTime);
            }
        }
        else if (GameObject.Find("Player").transform.position.x <= -7.2)
        {
            if (Input.GetKey(KeyCode.RightArrow))
            {
                transform.Translate(new Vector3(1, 0, 0) * speed * Time.deltaTime);
            }
        }
    }
}
