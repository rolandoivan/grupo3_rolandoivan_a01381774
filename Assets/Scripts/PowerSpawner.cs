﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerSpawner : MonoBehaviour
{
    public GameObject PrefabPower;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(EnemySpawner());
    }

    IEnumerator EnemySpawner()
    {
        while (true)
        {
            GameObject.Instantiate(PrefabPower, new Vector3(Random.Range(-8.0f, 8.0f), 6, 0), Quaternion.identity);
            yield return new WaitForSeconds(15);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
